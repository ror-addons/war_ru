War_RU = {};
War_RU.Items = {};
War_RU.Player = {};
War_RU.Guild = {};
War_RU.Quests = {};
function War_RU.Initialize()
EA_ChatWindow.Print(L"\x041f\x0430\x0440\x0441\x0435\x0440 wardb.ru \x0440\x0430\x0431\x043e\x0442\x0430\x0435\x0442.");
RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, "War_RU.ParseInventory");
War_RU.ParsePlayer();
War_RU.ParseGuild();
War_RU.ParseInventory();
War_RU.ParseQuests();
War_RU.uptime = 0;
War_RU.timeLeft = 1;
end
function War_RU.Update(elapsed)
War_RU.timeLeft = War_RU.timeLeft - elapsed;
if (War_RU.timeLeft > 0) then
return;
end
War_RU.timeLeft = 1;
War_RU.uptime = War_RU.uptime + 1;
if War_RU.uptime % 60 == 0 then
War_RU.ParsePlayer();
War_RU.ParseGuild();
end
end
function War_RU.FormatItemData(itemData)
bonus = {};
for i,b in pairs(itemData.bonus) do
bonus[i] = {b.type,b.value,b.reference};
end
item = {itemData.name,itemData.description,itemData.iLevel,itemData.level,itemData.iconNum,itemData.rarity,itemData.equipSlot,itemData.type,itemData.dps,itemData.speed,itemData.armor,itemData.renown,itemData.itemSet,itemData.craftingSkillRequirement,itemData.cultivationType,itemData.maxEquip,itemData.bop,itemData.sellPrice,itemData.blockRating,itemData.marketingIndex,itemData.marketingVariation,itemData.races,itemData.careers,itemData.slots,itemData.skills,bonus,itemData.enhSlot,itemData.numEnhancementSlots,itemData.trophyLocation,itemData.trophyLocIndex,itemData.broken,itemData.id};
War_RU.Items[itemData.uniqueID] = item;
end
function War_RU.ParsePlayer()
local tname = WStringToString(GameData.Player.name);
local renown = {GameData.Player.Renown.curRank,GameData.Player.Renown.curTitle,GameData.Player.Renown.curRenownEarned,GameData.Player.Renown.curRenownNeeded};
local xp = {GameData.Player.level,GameData.Player.Experience.curXpEarned,GameData.Player.Experience.curXpNeeded};
local equipment = {};
for itindex,item in pairs(GetEquipmentData()) do
equipment[itindex] = item.uniqueID;
end
for slot=1,CharacterWindow.numOfTrophiesUnlocked do
if (CharacterWindow.trophyData[slot].uniqueID > 0) then
equipment[slot+20] = CharacterWindow.trophyData[slot].uniqueID;
if (not War_RU.Items[CharacterWindow.trophyData[slot].uniqueID]) then
War_RU.FormatItemData(CharacterWindow.trophyData[slot]);
end
else
equipment[slot+20] = 0;
end
end
stats = {};
for k,v in pairs(GameData.Player.Stats) do
stats[k] = {v.baseValue,GetBonus(k, v.baseValue)};
end
local realm = WStringToString(SystemData.Server.Name);
if (realm == "Ariel") then
realm = 1;
elseif (realm == "Ranald") then
realm = 2;
elseif (realm == "Shallya") then
realm = 3;
else
realm = 0;
end
p = {realm,GameData.Player.name,GameData.Player.lastname,GameData.Player.race.id,GameData.Player.career.line,renown,xp,equipment,stats,GameData.Player.armorValue,GameData.Guild.m_GuildID};
if (not War_RU.Player[realm]) then
War_RU.Player[realm] = {};
War_RU.Player[realm][tname] = p;
else
War_RU.Player[realm][tname] = p;
end
end
function War_RU.ParseInventory()
for i, itemData in ipairs(CharacterWindow.equipmentData) do
if (itemData.uniqueID > 0) then
if (not War_RU.Items[itemData.uniqueID]) then
War_RU.FormatItemData(itemData);
end
end
end
for i, itemData in ipairs(DataUtils.GetItems()) do
if (itemData.uniqueID > 0) then
if (not War_RU.Items[itemData.uniqueID]) then
War_RU.FormatItemData(itemData);
end
end
end
for i, itemData in ipairs(DataUtils.GetQuestItems()) do
if (itemData.uniqueID > 0) then
if (not War_RU.Items[itemData.uniqueID]) then
War_RU.FormatItemData(itemData);
end
end
end
end
function War_RU.ParseGuild()
local guildMembersData = {};
local guildMembers = {};
local guild = {};
guildMembersData = GetGuildMemberData();
for k,v in pairs(guildMembersData)do
guildMembers[k] = {v.name,v.rank,v.careerID,v.titleString,v.statusNumber,v.lastLogin};
end
local realm = WStringToString(SystemData.Server.Name);
if (realm == "Ariel") then
realm = 1;
elseif (realm == "Ranald") then
realm = 2;
elseif (realm == "Shallya") then
realm = 3;
else
realm = 0;
end
guild = {realm,GameData.Guild.m_GuildName,GameData.Guild.m_GuildRank,GameData.Guild.m_GuildExpCurrent,GameData.Guild.m_GuildExpNeeded,GameData.Guild.m_GuildExpInCurrentLevel,GameData.Guild.m_GuildRenown,GameData.Guild.m_GuildCreationDateDay,GameData.Guild.m_GuildCreationDateMonth,GameData.Guild.m_GuildCreationDateYear,GameData.Guild.m_GuildBannersCaptured,GameData.Guild.m_GuildBannersLost,GameData.Guild.m_GuildWebsite,GameData.Guild.m_GuildEmail,guildMembers};
if (GameData.Guild.m_GuildID > 0) then
if (not War_RU.Guild[realm]) then
War_RU.Guild[realm] = {};
end
War_RU.Guild[realm][GameData.Guild.m_GuildID] = guild;
end
end
function War_RU.ParseQuests()
local tempQuests = DataUtils.GetQuests();
for i, questData in ipairs(tempQuests) do
local conds = {};
local k = 1;
for i,b in pairs(questData.conditions) do
conds[k] = {b.name,b.maxCounter};
k = k + 1;
end
local questRewards = GameData.GetQuestRewards(questData.id);
local rewards = {};
local itemsGiven = {};
for i, itemData in ipairs(questRewards.itemsGiven) do
if (itemData.uniqueID > 0) then
itemsGiven[i] = itemData.uniqueID;
if (not War_RU.Items[itemData.uniqueID]) then
War_RU.FormatItemData(itemData);
end
end
end
local itemsChosen = {};
for i, itemData in ipairs(questRewards.itemsChosen) do
if (itemData.uniqueID > 0) then
itemsChosen[i] = itemData.uniqueID;
if (not War_RU.Items[itemData.uniqueID]) then
War_RU.FormatItemData(itemData);
end
end
end
rewards[1] = questRewards.money;
rewards[2] = questRewards.xp;
rewards[3] = itemsGiven;
rewards[4] = questRewards.maxChoices;
rewards[5] = itemsChosen;
War_RU.Quests[questData.id] = {questData.name,questData.startDesc,questData.journalDesc,questData.maxTimer,questData.questTypes,conds,rewards,GameData.Player.name,GameData.Player.career.line};
end
end