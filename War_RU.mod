<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">  
  <UiMod name="War_RU" version="1.3.4" date="01/05/2010" >    
    <Author name="Saacy and Sooys" email="admin@wardb.ru" />    
    <Description text="War_RU" />       
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />  		
    <Dependencies>			
      <Dependency name="EA_CharacterWindow" />		
    </Dependencies>    
    <Files>      
      <File name="War_RU.lua" />    
    </Files>    
    <OnInitialize>      
      <CallFunction name="War_RU.Initialize" />    
    </OnInitialize>    
    <OnUpdate>      
      <CallFunction name="War_RU.Update" />    
    </OnUpdate>     
    <SavedVariables>               
      <SavedVariable name="War_RU.Items" global="true" />               
      <SavedVariable name="War_RU.Player" global="true" />               
      <SavedVariable name="War_RU.Guild" global="true" />               
      <SavedVariable name="War_RU.Quests" global="true" />    
    </SavedVariables>  
  </UiMod>
</ModuleFile>